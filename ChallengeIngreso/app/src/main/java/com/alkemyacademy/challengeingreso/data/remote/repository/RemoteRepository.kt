package com.alkemyacademy.challengeingreso.data.remote.repository;

import com.alkemyacademy.challengeingreso.data.remote.datasource.IRemoteDataSource
import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseRating
import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseSessionId
import com.alkemyacademy.challengeingreso.domain.ObjectResult
import com.alkemyacademy.challengeingreso.domain.entities.ResponseAppApi
import com.alkemyacademy.challengeingreso.domain.repository.IRemoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RemoteRepository(val remoteDataSource: IRemoteDataSource) : IRemoteRepository {
    override suspend fun getPopularMovies(): ObjectResult<ResponseAppApi> =
        withContext(Dispatchers.IO) {
            when (val result = this@RemoteRepository.remoteDataSource.getPopularMovies()) {
                is ObjectResult.Success -> ObjectResult.Success(result.data)
                is ObjectResult.Failure -> ObjectResult.Failure(result.exception)
            }
        }

    override suspend fun postRating(id: String, body: String, guestId: String): ObjectResult<ResponseRating> =
        withContext(Dispatchers.IO) {
            when (val result = this@RemoteRepository.remoteDataSource.postRating(id,body,guestId)) {
                is ObjectResult.Success -> ObjectResult.Success(result.data)
                is ObjectResult.Failure -> ObjectResult.Failure(result.exception)
            }
        }

    override suspend fun getGuestId(): ObjectResult<ResponseSessionId> =
        withContext(Dispatchers.IO){
            when (val result = this@RemoteRepository.remoteDataSource.getGuestId()) {
                is ObjectResult.Success -> ObjectResult.Success(result.data)
                is ObjectResult.Failure -> ObjectResult.Failure(result.exception)
            }
        }
}
