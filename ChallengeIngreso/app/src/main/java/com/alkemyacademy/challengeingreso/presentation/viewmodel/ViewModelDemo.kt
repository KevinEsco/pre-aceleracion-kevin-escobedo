package com.alkemyacademy.challengeingreso.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alkemyacademy.challengeingreso.domain.ObjectResult
import com.alkemyacademy.challengeingreso.domain.entities.Movie
import com.alkemyacademy.challengeingreso.domain.usecases.MoviesUseCases
import kotlinx.coroutines.launch

class ViewModelDemo(private val moviesUseCases: MoviesUseCases): ViewModel() {

    private val mListMovies = MutableLiveData<List<Movie>>()
    val listMovies: LiveData<List<Movie>> = mListMovies
    var guestId : String = ""

    private val mOnError = MutableLiveData<String>()
    val onError: LiveData<String> = mOnError


    fun getPopularMovies(){
        viewModelScope.launch {
            when(val result = this@ViewModelDemo.moviesUseCases.getPopularMovies()){
                is ObjectResult.Success -> this@ViewModelDemo.mListMovies.value = result.data.results
                is ObjectResult.Failure -> this@ViewModelDemo.mOnError.value = result.exception.message
            }
        }
    }

    fun filterMovies(filteredMovies:List<Movie>){
        viewModelScope.launch {
            this@ViewModelDemo.mListMovies.value = filteredMovies
        }
    }

    fun postRating(id: String, Body: String, guestId: String) {
        viewModelScope.launch {
            this@ViewModelDemo.moviesUseCases.postRating(id, Body, guestId)
        }
    }
    fun getGuestId(){
        viewModelScope.launch {
            when(val result = this@ViewModelDemo.moviesUseCases.getGuestId()){
                is ObjectResult.Success -> this@ViewModelDemo.guestId = result.data.guest_session_id
                is ObjectResult.Failure -> this@ViewModelDemo.mOnError.value = result.exception.message
            }
        }
    }
}