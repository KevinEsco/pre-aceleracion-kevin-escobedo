package com.alkemyacademy.challengeingreso.presentation.ui


import com.alkemyacademy.challengeingreso.domain.entities.Movie
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alkemyacademy.challengeingreso.databinding.ItemMovieBinding

typealias MovieClick = (movie: Movie) -> Unit
class MoviesAdapter: RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>() {

    var list: List<Movie> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var movieClick: MovieClick ?= null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesAdapter.MoviesViewHolder = MoviesViewHolder(ItemMovieBinding.inflate(
        LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: MoviesAdapter.MoviesViewHolder, position: Int) {
        holder.bind(this.list[position])
    }

    override fun getItemCount(): Int = this.list.size

    inner class MoviesViewHolder(private val binding: ItemMovieBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(movie: Movie){
            binding.apply {
                this.tvTitle.text = movie.title
                this.tvReleaseDate.text = movie.releaseDate
                this.tvVoteAverage.text = movie.vote_average
                this.clMovie.setOnClickListener {
                    this@MoviesAdapter.movieClick?.let {
                        it(movie)
                    }
                }
            }
        }
    }
}