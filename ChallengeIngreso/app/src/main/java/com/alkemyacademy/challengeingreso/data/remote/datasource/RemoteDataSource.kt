package com.alkemyacademy.challengeingreso.data.remote.datasource

import com.alkemyacademy.challengeingreso.data.remote.MoviesApiClient
import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseRating
import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseSessionId
import com.alkemyacademy.challengeingreso.data.remote.entities.responses.toResponseAppApi
import com.alkemyacademy.challengeingreso.domain.ObjectResult
import com.alkemyacademy.challengeingreso.domain.entities.ResponseAppApi

class RemoteDataSource(): IRemoteDataSource {

    override suspend fun getPopularMovies(): ObjectResult<ResponseAppApi> {
        val appApi = MoviesApiClient.build()

        return try{
            val response = appApi.getPopularMovies()
            if (response.isSuccessful){
                val body = response.body()
                ObjectResult.Success(body?.toResponseAppApi() ?: ResponseAppApi())
            }else{
                ObjectResult.Failure(Exception(response.errorBody()?.toString()))
            }
        } catch (ex: java.lang.Exception){
            ObjectResult.Failure(ex)
        }
    }


    override suspend fun postRating(id: String, body: String, guestId: String): ObjectResult<ResponseRating> {
        val appApi = MoviesApiClient.build()

        return try{
            val response = appApi.postRating(id , body, guestId)
            if (response.isSuccessful){
                ObjectResult.Success(response?.body() ?: ResponseRating("2","error"))
            }else{
                ObjectResult.Failure(Exception(response.errorBody()?.toString()))
            }
        }catch (ex:java.lang.Exception){
            ObjectResult.Failure(ex)
        }
    }

    override suspend fun getGuestId(): ObjectResult<ResponseSessionId> {
        val appApi = MoviesApiClient.build()
        return try{
            val response = appApi.getGuestId()
            if(response.isSuccessful){
                ObjectResult.Success(response?.body() ?:ResponseSessionId("",""))
            }else{
                ObjectResult.Failure(Exception(response.errorBody()?.toString()))
            }
        } catch(ex: java.lang.Exception){
            ObjectResult.Failure(ex)

        }
    }


}