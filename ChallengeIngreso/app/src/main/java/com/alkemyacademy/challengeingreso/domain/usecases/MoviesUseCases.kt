package com.alkemyacademy.challengeingreso.domain.usecases;

import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseSessionId
import com.alkemyacademy.challengeingreso.data.remote.repository.RemoteRepository
import com.alkemyacademy.challengeingreso.domain.ObjectResult
import com.alkemyacademy.challengeingreso.domain.entities.ResponseAppApi

class MoviesUseCases(val remoteRepository: RemoteRepository) {
    suspend fun getPopularMovies(): ObjectResult<ResponseAppApi> = this.remoteRepository.getPopularMovies()
    suspend fun postRating(id:String, body:String, guestId:String) = this.remoteRepository.postRating(id, body, guestId)
    suspend fun getGuestId(): ObjectResult<ResponseSessionId> = this.remoteRepository.getGuestId()
}
