package com.alkemyacademy.challengeingreso.domain.entities

data class ResponseAppApi(val results: List<Movie> ?= emptyList())
data class Movie(val title: String, val overview:String,val releaseDate: String, val posterPath: String, val vote_average:String, val id:String)
