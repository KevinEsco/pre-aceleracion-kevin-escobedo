package com.alkemyacademy.challengeingreso.data.remote.datasource;

import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseRating
import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseSessionId
import com.alkemyacademy.challengeingreso.domain.ObjectResult
import com.alkemyacademy.challengeingreso.domain.entities.ResponseAppApi;

public interface IRemoteDataSource {
    suspend fun getPopularMovies(): ObjectResult<ResponseAppApi>
    suspend fun getGuestId(): ObjectResult<ResponseSessionId>
    suspend fun postRating(id: String, body: String, guestId: String): ObjectResult<ResponseRating>
}
