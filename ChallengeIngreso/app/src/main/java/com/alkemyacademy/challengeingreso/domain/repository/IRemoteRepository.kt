package com.alkemyacademy.challengeingreso.domain.repository

import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseRating
import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseSessionId
import com.alkemyacademy.challengeingreso.domain.ObjectResult
import com.alkemyacademy.challengeingreso.domain.entities.ResponseAppApi

interface IRemoteRepository {
    suspend fun getPopularMovies(): ObjectResult<ResponseAppApi>
    suspend fun postRating(id: String, body: String, guestId: String): ObjectResult<ResponseRating>
    suspend fun getGuestId(): ObjectResult<ResponseSessionId>

}