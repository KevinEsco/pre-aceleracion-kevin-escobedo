package com.alkemyacademy.challengeingreso.presentation.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.SearchView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel

import com.alkemyacademy.challengeingreso.data.remote.datasource.RemoteDataSource
import com.alkemyacademy.challengeingreso.data.remote.repository.RemoteRepository
import com.alkemyacademy.challengeingreso.databinding.ActivityMainBinding
import com.alkemyacademy.challengeingreso.domain.entities.Movie
import com.alkemyacademy.challengeingreso.domain.usecases.MoviesUseCases
import com.alkemyacademy.challengeingreso.presentation.viewmodel.ViewModelDemo
import com.alkemyacademy.challengeingreso.presentation.viewmodel.ViewModelFactory
import java.util.*
import javax.sql.DataSource


class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    private lateinit var tempArrayList: MutableList<Movie>
    private lateinit var newArrayList: MutableList<Movie>
    private lateinit var newArrayListG: MutableList<Movie>
    private val viewModel by viewModels<ViewModelDemo>{
        val remoteRepository = RemoteRepository(RemoteDataSource())
        ViewModelFactory(MoviesUseCases(remoteRepository))
    }

    private val adapter: MoviesAdapter by lazy { MoviesAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()
        callViewModelMethods()
        initRecycler()
        viewModelObservers()
        tempArrayList = arrayListOf<Movie>()
        this.binding.svMovies.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false

            }
            override fun onQueryTextChange(newText: String?): Boolean {

                tempArrayList.clear()
                val searchText = newText!!.lowercase(Locale.getDefault())
                if (searchText.isNotEmpty()){
                    newArrayList.forEach {
                        if(it.title.lowercase(Locale.getDefault()).contains(searchText)){
                            tempArrayList.add(it)
                        }
                    }
                    filterViewmodel(tempArrayList)
                }else{
                    tempArrayList.clear()
                    reloadViewodel()
                    filterViewmodel(tempArrayList)
                }
                return false
            }

        })
    }


    private fun reloadViewodel() {
        this.viewModel.getPopularMovies()

    }

    private fun filterViewmodel(filteredMovies:List<Movie>) {
        this.viewModel.filterMovies(filteredMovies)
    }

    private fun bind() {
        this.binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(this.binding.root)
    }
    private fun viewModelObservers(){

        this.viewModel.onError.observe(this,{
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        this.viewModel.listMovies.observe(this, {
            this.adapter.list = it
            this.newArrayList = it as MutableList<Movie>
        })
    }
    private fun callViewModelMethods(){
        this.viewModel.getPopularMovies()
    }

    private fun initRecycler(){
        this.binding.rvMovies.adapter = this.adapter

        this.adapter.movieClick = {
            var IntentMovie: Intent = Intent(this, Activity_Movie::class.java)
            IntentMovie.putExtra("title", it.title)
            IntentMovie.putExtra("overview", it.overview)
            IntentMovie.putExtra("posterPath", it.posterPath)
            IntentMovie.putExtra("id", it.id)
            startActivity(IntentMovie)
        }
    }
}

